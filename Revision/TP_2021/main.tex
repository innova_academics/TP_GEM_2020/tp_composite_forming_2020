\documentclass{exam}
\usepackage[T1]{fontenc}

\title{\textbf{TP formage des composites}}
\date{}
\author{Ecole Centrale de Nantes, Departement, année}

\usepackage[french]{babel}	
\usepackage[dvipsnames]{xcolor}
\usepackage{graphicx}
\usepackage[hmargin=1in,vmargin=2cm]{geometry}
\usepackage{indentfirst}
\usepackage{float}
\usepackage{subcaption}
\usepackage{wrapfig}
\usepackage{amsmath}
\usepackage{etoolbox}
\usepackage{booktabs}
\patchcmd{\thebibliography}{\section*{\refname}}{}{}{}

\begin{document}

\maketitle

\section{Mise en contexte}

    \par Vous êtes ingénieur et vous devez produire une pièce en matériau composite de géométrie "non développable" ou "fortement non développable". 
    Le procédé de mise en forme utilisé pour fabriquer cette pièce est le moulage par transfert de résine (RTM) dont les phases sont décrites Fig.1.
 
    \begin{figure}[h!]    
    \centering
    \includegraphics[scale=0.9]{img/Process.PNG}
    \caption{Processus de mise en forme d'un matériau composite à renfort tissé.}
    \cite{Boisse}
    \label{fig:Process}
    \end{figure}

    \par Vous vous intéressez à l'étape a) de la Fig.1 au cours de laquelle le renfort fibreux est mis en forme. Vous souhaitez étudier sa déformation pour prédire l'orientation finale des fibres et vérifier l'absence de défauts sur le renfort formé.
    La forme à obtenir est un "double dôme" présenté Fig.2.
    
    \begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{img/Double-dome.png}
    \caption{Renfort tissé sur un double dôme (expérimental et numérique), \cite{Pierce}}
    \label{fig:Double-dome}
    \end{figure}

    \par Dans ce contexte, un modèle décrivant le comportement du renfort au cours de son formage a été développé au sein de votre entreprise. Le prix des matériaux mis en jeu ne permet effectivement pas économiquement de multiplier les essais expérimentaux. Vous vous apprêtez à lancer une nouvelle simulation.

    \vspace{5mm}

    \begin{center}
    \begin{Large}
    \par \textbf{Votre objectif: Valider et exploiter la simulation du comportement du renfort texile au cours du formage}
    \end{Large}
    \end{center}

\newpage

    \par En tant qu'ingénieur, vous savez que le cisaillement plan est considéré comme le principal mécanisme de déformation durant la mise en forme d'un renfort sur une géométrie non développable \cite{Boisse}. Le \textbf{cisaillement plan} traduit la rotation entre deux mèches autour de leur point de croisement. En effet la rigidité du tissu en cisaillement est faible par rapport à sa rigidité en tension. On observe sur la figure \ref{fig:simulation} que la prise en compte du \textbf{cisaillement} est extrêmement importante dans la simulation pour reproduire ces effets. Vous savez donc qu'il est primordial d'étudier puis de caractériser ce mode de déformation afin de développer un modèle de comportement. Une caractérisation des propriétés du matériau pour ce mode de sollicitation est donc indispensable.

    \begin{figure}[h!]
    \centering
    \includegraphics[scale=0.4]{img/Simulation.png}
    \caption{Déformées d'un dôme \cite{Boisse}}
    \label{fig:simulation}
    \end{figure}
    
    \par Pour effectuer cette étude votre travail est divisé en plusieurs étapes :
    \begin{itemize}
        \item \textbf{Caractériser le matériau textile} grâce aux données d'un essai mécanique sur éprouvette issu d'un article publié par vos pairs. Vous comptez exploiter ces mesures avec un code Matlab.
        \item \textbf{Réaliser la simulation numérique} de l'essai avec un maillage éléments finis et les caractéristiques précédemment obtenues.
        \item \textbf{Visualiser l'essai simulé} avec le logiciel Paraview et \textbf{comparer le profil de déformation} avec des résultats expérimentaux à disposition dans votre bibliographie.
    \end{itemize}
    
\newpage
\clearpage
\section{Caractérisation du renfort}

    Vous souhaitez retrouver les caractéristiques du matériau utilisé à partir de l'analyse des essais de cisaillement réalisés par vos pairs. L'objectif ici est donc de trouver ce qui relie les contraintes aux déformations dans les essais de cisaillement étudiés.
        
        \subsection{Essai réalisé}
                
                \par Il y a plusieurs façons de mesurer expérimentalement le cisaillement, notamment le "picture frame test" et le "bias extension test". Le premier consiste à fixer un tissu dans un cadre puis à le déformer pour faire apparaître du cisaillement, cependant cette méthode n'est pas optimale car le résultat dépend fortement de la méthode d'attache au cadre. On utilise donc ici le "bias extension test" qui consiste en un essai de traction sur un tissu dont les fils sont orientés à +/- 45° (par rapport au repère de la machine). 
                               
                \begin{figure}[h]
                        \centering                       
                        \includegraphics[scale=0.6]{img/Bias test.PNG}
                        \caption{Représentation de l'essai de cisaillement pur \cite{Guzman}}
                \end{figure}

                \par On observe trois zones différentes: la zone centrale A qui est sollicitée en cisaillement pur, les zones latérales B qui sont demi-cisaillées, et les zones C aux extrémités de l'éprouvette qui ne sont pas cisaillées.

        \subsection{Explication : Quels paramètres déterminer ?}
    

                \par Nous proposons d'étudier le comportement du textile (\textit{plain weave} ou \textit{toile}) en adoptant un modèle hyperélastique, dans lequel il est supposé que la transformation peut se décomposer multiplicativement en trois transformations distinctes : l'allongement dans la direction du fil de trame, l'allongement dans la direction du fil de chaîne, puis enfin le cisaillement.

                \begin{figure}[H]
                    \hspace*{\fill}
                    \begin{subfigure}[b]{0.25\textwidth}
                    \includegraphics[width=\linewidth]{img/Mode1.pdf}
                    \caption{Elongation chaine}
                    \end{subfigure}
                    \hspace*{\fill}% this is optional just to fix the horizontal space between the two images
                    \begin{subfigure}[b]{0.25\textwidth}
                        \includegraphics[width=\linewidth]{img/Mode2.pdf}
                        \caption{Elongation trame}
                    \end{subfigure}
                    \hspace*{\fill}% this is optional just to fix the horizontal space between the two images
                    \begin{subfigure}[b]{0.25\textwidth}
                        \includegraphics[width=\linewidth]{img/Mode3.pdf}
                        \caption{Cisaillement dans le plan }
                    \end{subfigure}
                    \hspace*{\fill}
                    \caption{Principaux modes de déformation \cite{Guzman}}
                \end{figure} 

                Chaque mode de déformation est identifiable par un invariant exprimé à l'aide des invariants: 

                \begin{equation*} I_{elong1}, I_{elong2}, I_{sh} \end{equation*}

                \par La densité d'énergie de déformation totale s'exprime alors selon une somme de densités d'énergie de déformation, chacune associées à une transformation. En effet, selon l'hypothèse de découplage :
                \begin{equation}
                    W = W_{elong1}+W_{elong2}+W_{sh}
                \end{equation}

                \par Chacune de ces densités ne dépend que de l'invariant associé à la transformation. Ainsi $W_{sh}$ ne dépend que de $I_{sh} = sin(\gamma)$. Pourquoi chercher à déterminer cette énergie de déformation ? Dans le cas d'un modèle hyperélastique, la loi de comportement du matériau peut se caractériser à l'aide des tenseurs de Cauchy, Piola-Kirchoff I et II. On considère ici le tenseur PK II, noté S. Ces tenseurs sont liés aux tenseurs cinématiques (qu'on connaît) par une relation faisant intervenir l'énergie de déformation. Par définition en effet :

                \begin{equation}
                    \underline{\underline{S}} = 2\frac{\partial W}{\partial \underline{\underline{C}}}
                \end{equation}
                
                \par On comprend donc que le tenseur S lui-même peut s'écrire sous la forme de trois composantes chacune reliées à une seule transformation. Si l'on se place au centre de l'éprouvette - celle-ci étant soumise à un cisaillement pur - (cf. figure 4) seule la composante de cisaillement est nécessaire :

                \begin{equation}
                    \underline{\underline{S}}_{sh} = 2\frac{\partial W_{sh}(I_{sh})}{\partial \underline{\underline{C}}}
                \end{equation}

                \par L'objectif de cette partie est donc de déterminer la densité d'énergie de cisaillement, qui permettra ensuite de décrire la réponse en cisaillement du textile considéré. On fait ici le choix d'un modèle permettant de l'exprimer sous la forme d'un polynôme de $I_{sh}$, tel que :
                \begin{equation}
                    W_{sh} = \sum\limits_{i=1}^{n_{sh}}k_{shi}(I_{sh})^{2i}
                \end{equation}
                \par Ici $n_{sh} = 3$. La densité est ainsi déterminée par 3 paramètres $k_{sh1}$, $k_{sh2}$ et $k_{sh3}$. Il convient maintenant de déterminer les valeurs de ces trois paramètres, ce que vous allez faire à l'aide d'un algorithme.

        
        \subsection{Explication : Détermination de l'effort approché}
                
                Une étude énergétique de l'essai permet de relier les composantes du tenseur $\underline{\underline{S}}_{sh}$ à l'effort machine $F_{machine}$. Cette expression est utilisée dans l'algorithme. Son utilisation est comparée aux données mesurées lors de l'expérimentation et permet ainsi le calcul d'optimisation. On note $S_A $ la surface initiale de la zone A, $S_B$ celle de la zone B, et enfin $h_0$ l'épaisseur initiale. S11 correspond à l'axe horizontal, S22 à l'axe vertical :
                
                \begin{equation}
                    {F_{machine} = S_{A}\frac{h_{0}}{\sqrt{2}D}\frac{cos(\gamma)}{sin(\frac{\alpha}{2})}(S_{22}-S_{11})_{zone A} +S_{B}\frac{h_{0}}{2\sqrt{2}D}\frac{cos(\frac{\gamma}{2})}{sin(\frac{\alpha}{2})}(S_{22}-S_{11})_{zone B}}
                \end{equation}
        
        \subsection{ Explication : Algorithme d'optimisation des paramètres}
        
                Vous avez à votre disposition un code Matlab de l'algorithme de Levenberg-Marquardt, ainsi qu'un logigramme en annexe vous permettant de comprendre son architecture. La variable p contient les coefficients à optimiser ; l'erreur est pondérée par une énergie de déformation surfacique. Pour une minimisation de l'erreur aux moindres carrés, il suffirait de modifier $w_i $ par 1.
                
                
                \begin{equation}
                    \xi^2(\underbar p)= \frac{1}{2} \sum\limits_{i=1}^{N} \left[ \frac{F_{exp} \left(d(t)\right)-F_{mod}\left(d(t), \underbar p\right)}{w_i} \right]^2
                \end{equation}
                                
        \subsection{Travail : Optimisation des paramètres}
        
            Le fichier "Data\_Bias\_test.txt" contient les mesures effectuées lors d'un essai de traction en biais du textile que nous considérons. Ces mesures sont issues de l'article de \cite{Khan}. Les programmes Matlab à utiliser pour calculer puis extraire les paramètres matériaux se trouvent dans le dossier "Algorithme".
            
            \vspace{3mm}
            
            \begin{questions}
                \question\textbf{Complétez le fichier "F\_Bias\_Test.m" avec les bonnes dimensions de l'éprouvette et exécuter le fichier "Main\_Bias\_Test.m" pour retrouver les valeurs des coefficients du vecteur \underbar p. }
                \vspace{1mm}
                \question\textbf{En déduire les caractéristiques matériaux pertinentes pour la simulation numérique.}
            \end{questions}
            
            \vspace{5mm}
            
            \begin{table}[h]
                \centering
                    \begin{tabular}{lc}
                        \toprule
                        $k_{shear}$ & Valeur \\ 
                        \midrule
                        $k_{sh1}$ & \hspace*{2cm} \\ 
                        $k_{sh2}$ & \hspace*{2cm} \\ 
                        $k_{sh3}$ & \hspace*{2cm} \\ 
                        \bottomrule
                    \end{tabular}
            \end{table}


            
            %Utiliser ici :
            %- Fichier .docx qui explique la méthode de caractérisation ;
            %- Fichier source de données d'essais
            %- Fichiers octave servant d'algorithme

\section{Implémentation}

    \par Dans cette partie, vous avez accès au fichier de données d'entrée, à modifier. Rendez-vous dans le fichier \texttt{double\char`_dome\char`_forming.bim}. Celui-ci prend en entrée les caractéristiques du tissu et donne en sortie un maillage qui vous servira lors la simulation.
    \vspace{3mm}
                      
            
        \begin{questions}
            \setcounter{question}{2}
            \question \textbf{A l'aide de la première annexe du sujet de TP, renseignez-vous sur le contenu du fichier .bim considéré, afin de comprendre son architecture. }

            \vspace{1mm}

            \question \textbf{Où renseigner les caractéristiques matériau obtenues précédemment?}
            
            \vspace{1mm}

            \question \textbf{Regardez les raideurs d'élongation renseignées dans le .bim. Sont-elles en accord avec la réalité physique? Faut-il les modifier? Même question pour la masse surfacique (RHO).}
            
        \end{questions}

\section{Simulation et comparaison aux données expérimentales}

            Dans cette partie nous allons utiliser le maillage conservé dans le fichier .bim pour faire une simulation. \textbf{Plaçez le fichier dans le dossier "Executable", lancez COMFOR.exe et écrivez le nom du fichier .bim en question.} Cela peut prendre plusieurs dizaines de minutes. 
            
            \vspace{3mm} 

            Une fois la simulation terminée vous obtenez un dossier de fichiers d'extension .vtk. Ces fichiers ont un format adapté à la visualisation des données et au post-traitement, on utilise pour cela le logiciel Paraview. 
            
            \vspace{3mm}

            \textbf{La seconde annexe (vidéo) vous permet de découvrir Paraview. Il faut maintenant exploiter les données de l'essai de formage du tissu.}

        \newpage
            \begin{figure}[h!]
            \centering
            \includegraphics[scale=0.7]{img/dd 90.PNG}
            \caption{Exemple d'expérience sur un double dôme \cite{Khan}}
            \label{fig:dd 90}
        \end{figure}
    
            La figure \ref{fig:dd 90} correspond à une expérience de formage d'un double dôme avec les mêmes paramètres que ceux choisis pour la simulation. Dans cet article de bibliographie, les chercheurs tracent une ligne passant par le plus grand et le plus petit angle de cisaillement, pour pouvoir observer l'étendue de valeurs.
            Utiliser Paraview pour reproduire le graphe de la figure \ref{fig:coupe} qui reporte l'angle de cisaillement le long de la ligne de la figure \ref{fig:dd 90} afin de la comparer à l'essai de la bibliographie.

        \begin{figure}[h!]
            \centering
            \includegraphics[scale=0.7]{img/dd 90 courbe.PNG}
            \caption{Angle de cisaillement sur la ligne définie précédemment \cite{Khan}}
            \label{fig:coupe}
        \end{figure}

        \textbf{Pour tracer l'angle de cisaillement le long de cette ligne, regardez le tutoriel Paraview vidéo disponible avec le TP.}

        \vspace{1mm}
            
        \begin{questions}
            \setcounter{question}{5}
            \question \textbf{Comparez la figure obtenue sur Paraview avec celle de la bibliographie (expérimentale).}
            \question \textbf{Conclure sur la pertinence de la simulation.}
        \end{questions}

\section{Discussion}

            A la lumière de vos résultats numériques, discutez des zones possibles d'apparition de défauts. Proposer un paramètre dont vous évaluerez l'influence sur la drapabilité (épaisseur, orientation des fibres, tissage).

%\section{Pour aller plus loin (facultatif)}

    %Si on a le temps, on pourra au moins mettre quelques lignes sur les choses qu'on aurait aimé leur faire faire mais pour lesquelles on a pas eu le temps. Ou les inviter à travailler sur un cas différent, ...


% ------------------------------------------------------> Biblio <---
%
\section{Références}
\renewcommand\bibname{}
\begin{thebibliography}{1}

        \bibitem{Boisse}
        P.~Boisse.
        \newblock {\em Mise en forme des renforts fibreux de composites}.
        \newblock Ed. Techniques Ing{\'e}nieur, 2004.

        \bibitem{Pierce}
        P.~Robert Samuel, 
        \newblock {\em Improving the process modelling capability for manufacturing large composite structures used on passenger aircraft}.
        \newblock PhD thesis, Monash University, 2014.
        
        \bibitem{Guzman}
        E.~Guzman-Maldonado.
        \newblock {\em Modélisation et simulation de la mise en forme des matériaux composites pré-imprégnés a matrice thermoplastique et a fibres continues}.
        \newblock PhD thesis, Université de Lyon, 2016.
        
        \bibitem{Khan}
        M.~A. Khan, T.~Mabrouki, and P.~Boisse.
        \newblock Numerical and experimental forming analysis of woven composites with
          double dome benchmark.
        \newblock {\em International Journal of Material Forming}, 2(S1):201--204,
          2009.
    
	% ----------------------------------------------------------------------
\end{thebibliography}
% ------------------------------------------------------> Biblio <---
%
\section{Annexe}
    \subsection{Logigramme de Lavenberg-Marquardt}
                \begin{figure}[H]
                    \centering
                    \includegraphics[scale=0.6]{img/LM_1.pdf}
                    \caption{Logigramme de Levenberg-Marquardt \cite{Guzman}}
                \end{figure}
    
    \subsection{Fichier .bim}

Afin de simuler le formage du textile considéré et de comprendre ce faisant les objets que vous manipulez, la structure du fichier d'entrée d'extension .bim est décrite ci-dessous.

\begin{itemize}
    \item § 1 : Les grandeurs de contrôle que sont le temps de simulation, le pas de temps et l'intervalle d'affichage.
    
    \item § 2 : Les données matériaux : \textcolor{Emerald}{"nom matériau"} RHO = \textcolor{Emerald}{"masse surfacique"} DAMPING = \textcolor{Emerald}{"amortissement numérique"} WARPORI = \textcolor{Emerald}{"vecteur directeur chaîne"} WEFTORI = \textcolor{Emerald}{"vecteur directeur trame"} KELONGWARP = \textcolor{Emerald}{"module d'élasticité de la chaîne [1,0,0] en formage sec"} KELONGWEFT = \textcolor{Emerald}{module d'élasticité de la trame [1,0,0] en formage sec} KSHEAR = \textcolor{Emerald}{"coefficients hyperélastiques associés au cisaillement"}. 
    
    \item § 3 : Les conditions limites acceptées sont des conditions de Dirichlet dont les caractéristiques sont données sous la forme (ici pour la coordonnée x): \textcolor{Emerald}{"nom de la BC"} VX = \textcolor{Emerald}{"vitesse suivant x"} (On peut ajouter des conditions d'accélération AX ou de vitesse angulaire VRX).
    
    \item § 4 : Liste les noeuds dans leurs configurations initiales sous la forme suivante: \textcolor{Emerald}{"numéro noeud"} X = \textcolor{Emerald}{"coordonnée x"} Y = \textcolor{Emerald}{"coordonnée y"} Z =  \textcolor{Emerald}{"coordonnée z"} Constraint = \textcolor{Emerald}{"nom de la contrainte"} (pour un noeud libre Constraint n'est pas renseigné).
    
    \item § 5 : Décrit les éléments: CONTACT\_TRIANGLE : \textcolor{Emerald}{"numéro élément"} nodes = \textcolor{Emerald}{"noeuds associés"} T = \textcolor{Emerald}{"épaisseur"} FACTOR = \textcolor{Emerald}{"facteur de rigidité surfacique"}.
    
    \item § 6 : Décrit les éléments: MEMBRANE\_3 : nodes = \textcolor{Emerald}{"noeuds associés"} MATERIAUX = \textcolor{Emerald}{"matériau de la membrane"} T = \textcolor{Emerald}{"épaisseur"} FACTOR = \textcolor{Emerald}{"facteur de rigidité surfacique"} CONTACT = \textcolor{Emerald}{??????}
    
\end{itemize}

    \begin{figure}[h!]
    \centering
    \includegraphics[scale=0.5]{img/bim.PNG}
    \caption{Exemple de fichier d'entrée de calcul sous COMFOR}
    \label{fig:bim}
    \end{figure}
    
     \subsection{Notice Paraview}
     
        Nous avons fait le choix de vous proposer un rapide tutorial vidéo pour vous permettre de prendre en main efficacement Paraview. Vous le trouverez dans le dossier du TP. Si vous souhaitez davantage de détails sur le fonctionnement de ce logiciel, il existe une documentation exhaustive sur internet.

\end{document}