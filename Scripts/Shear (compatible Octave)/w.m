function [out, dout]=w(Type,I,C)
% -------- INPUT VARIABLES ---------
%   Type = Type de potentielle
%        * 1 : Elongation sens chaine 
%        * 2 : Elongation sens trame
%        * 3 : Cisaillement plan
%   I    = m-vecteur contenant les invariants.
%   C    = Coefficients du potentielle.

if Type==1      
    out =C*I.^2;
    dout=2*C*I;
elseif Type==2
    out=C*I.^2;
    dout=2*C*I;
elseif Type==3
%Polynome degre n
    out =zeros(numel(I),1);
    dout=zeros(numel(I),1);
    for i=1:numel(I)
        for n=2:numel(C)+1
        out(i) = out(i) + C(n-1)*I(i)^(n);
        dout(i)= dout(i)+ n*C(n-1)*I(i)^(n-1);
        end 
    end
elseif Type==4
%Polynome pair degre n
    out =zeros(numel(I),1);
    dout=zeros(numel(I),1);
    for i=1:numel(I)
        for n=1:numel(C)
        out(i) = out(i) + C(n)*I(i)^(2*n);
        dout(i)= dout(i)+ 2*n*C(n)*I(i)^(2*n-1);
        end 
    end
end
end