%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
% Levenberg-Marquardt for the characterization of hyperelastic
% properties using the Bias extension test
% GUZMAN MALDONADO Eduardo,Ph.D Student
% LAboratoire de Mecanique des Contacts et des Structures (LaMCoS)
% Institut National des Sciences Apliqu�es de Lyon (INSA), Octobre 2017
%  Adapted to Octave on 03-Jan-2021 by Gr�goire Lebreton
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
clc
clear all
hold off
%--------------------------------------------------------------------------
% Parametres d'entr�e
filename ='Scripts_Shear_Example.txt' ;  %<<< INPUT
% Valeurs initiales 
% Coefficients � recaler.[Psh]
p_ini    = [1 ,1, 1];
% Permet de lancer des optimisations avec moins de points, utile pour les
% premieres optimisations
pas = 1;
% Poid de la mesure : 
%   1 = cas des moindr�s carr�s, meme poids pour l'ensemble des points
%   2 = on neglige l'influence des valeurs du premier pic
type_weight = 1;
%--------------------------------------------------------------------------
% Lecture des donn�es d'entr�e
% Input File Format.
% Temps (s)     Deplacement     Effort     Thermocuples i
%
% ...#...,      ...#...,        ...#...       ...#...
% ...#...,      ...#...,        ...#...       ...#...
%--------------------------------------------------------------------------
% Importation des donn�es.
%--------------------------------------------------------------------------
importfile(filename)
% Recup�ration du temps 
t_exp      = data(:,1)- data(1,1);    %Offset temps
delta_time = diff(t_exp);                 %Increment de temps [s]
%--------------------------------------------------------------------------
% Operation necessaire dans le cas o� les parametres de chauffe sont 
% inclus dans le meme fichier d'entr�e.
% Index du d�but de l'enregistrement des parametres du bias test
j = find(delta_time<5); index = j(1)+1 ;  
%--------------------------------------------------------------------------
% Offset deplacement
d_exp = [0;data(index+1:end,2)] ;                                  % [mm]
%--------------------------------------------------------------------------
% Offset Force
F_exp = [0;data(index+1:end,3)-data(index,3)];                % [N]
%--------------------------------------------------------------------------
% Nombre de points experimentaux
Np1   = numel(t_exp);
%--------------------------------------------------------------------------

%% Optimisation LM
%--------------------------------------------------------------------------
% Transformation en vecteur colonne
p_ini= p_ini(:); 
%--------------------------------------------------------------------------
% Definiton des grandeurs globales
p1 = numel(p_ini);   %Nombre des coeffcients du potentielle (cisaillement)

% Definiton des bornes pour l'optimisation
p_min = [0 -1*ones(1,p1-1)];
p_max = ones(1,p1);
%--------------------------------------------------------------------------
%Calcul du poids des mesures
w_mesure = ones(numel(d_exp(1:pas:end)),1);

% Lancement de l'algortime d'optimisation
[p_fit,Chi_sq,sigma_p,sigma_y,corr,R2,cvg_hst] = lm1('F_Bias_test',...
 p_ini,d_exp(1:pas:end),F_exp(1:pas:end), w_mesure ,0.001,p_min,p_max);
%--------------------------------------------------------------------------
% Fin de l'optimisation et affichage de l'historique de convergence
n = length(p_fit);
labels_caption = cell(1,n);
for i=1:n
    labels_caption{i}= strcat('p_',num2str(i));
end
plot(1:length(cvg_hst(:,1)),cvg_hst(:,1:n));
legend(labels_caption);
xlabel('Iteration number')
ylabel('Parameter values')
   
%% We create some map colors
 y = 0:0.15:0.8;
 x= [y y];
 Color_grey =[x; x; x]';
 Color_publi = [[0 0 0];[0 0 1];[0 0.5 0];[1 0 0]]; 
 Color_lines = lines(3);
 Colors_default = {'k','b',[0 0.5 0],'r',[1 1 1]*0.5}; 

%Markers 
num_Markers = 40; %<--------
LineTypes   = {'-.','-','--'};
%Markers = ['.','x','o','*','s','d','^','v','>','<','p','h','.'];
Markers = ['+','s','d','^','v','>','<','p','h','.'];

F_fit=F_Bias_test(d_exp,p_fit,0);
set_window('D�placement [mm] ','Load [N]','') 
line_fewer_markers(d_exp,F_exp,num_Markers , 'Marker','o','Color',Color_publi(1,:),'LineWidth',0.75,'MarkerSize',4,'spacing','x','LineStyle','none');
line_fewer_markers(d_exp,F_fit ,num_Markers , 'Marker','none','Color',Color_publi(2,:),'LineWidth',0.75,'MarkerSize',4,'spacing','x','LineStyle','-');

legend('Donn�es Exp.','Optimisation')