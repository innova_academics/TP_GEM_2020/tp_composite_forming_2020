classdef node
  properties
    nbr 
    x
    y
    z
    constraint
  
  end
  
  methods
    function n = node(nbr,x,y,z,constraint='none')
      n.nbr= nbr;
      n.x=x;
      n.y=y;
      n.z=z;
      n.constraint=constraint
    end
   end
end
