classdef material
  properties
    name
    rho 
    damping 
    warpori 
    weftori 
    kelongwarp
    kelongweft
    kshear  
  end
  
  methods
    function m = material(name='basis',rho=0.0001,damping=5.0,warpori = '[1,1,0]',weftori = '[-1,1,0]',kelongwarp= '[1,0,0]',kelongweft= '[1,0,0]',kshear='[0,0,0]')
      m.name= name;
      m.rho = rho;
      m.damping = damping;
      m.warpori = warpori;
      m.weftori = weftori;
      m.kelongwarp= kelongwarp;
      m.kelongweft= kelongweft;
      m.kshear  = kshear;
    end
   end
end
