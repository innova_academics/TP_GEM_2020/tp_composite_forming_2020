%% Routine to create innovamics' entry file "*.bim"
%% Written by G. Lebreton and N. Piecko - 20/01/2021
clear all
## Reading Abaqus-like file 
%code Nico%
nodes=dlmread("nodes.inp",",");

% 2N ELEMENTS %
T3D2_truss=dlmread("T3D2_truss.inp",",");
    % On suppose que ce sont des éléments pour appliquer des conditions aux limites %
T3D2_BR=dlmread("T3D2_BR.inp",",");
    % Je suppose que ce sont les éléments qu'on voit flotter en forme de Z %

% 4N ELEMENTS %
R3D4_die=dlmread("R3D4_die.inp",",");
    % Correspond au moule %
R3D4_binder=dlmread("R3D4_binder.inp",",");
    % Correspond à ce qui empêche des plis dans la feuille %
R3D4_punch=dlmread("R3D4_punch.inp",",");
    % Correspond au poinçon %
S4R_shell=dlmread("S4R_shell.inp",",");
    % Correspond à la feuille qui va être formée %

% Catégories de noeuds %
NSET_nsheet=dlmread("NSET_nsheet.inp",",");
NSET_rside=dlmread("NSET_rside.inp",",");
NSET_topside=dlmread("NSET_topside.inp",",");
NSET_xside=dlmread("NSET_xside.inp",",");
NSET_yside=dlmread("NSET_yside.inp",",");
  % Les noeuds nsheet sont probablement les noeuds de la membrane %
  % Ce sont des noeuds associés à des éléments truss et shell %
  % Certains des noeuds nsheet sont ensuite distingués en RIGHTSIDE, TOPSIDE, XSIDE et YSIDE %
  % Ce sont les noeuds en bordure de membrane : ils n ont que 2 ou 3 élements truss associés et %
  % un ou deux éléments shell ; ce qui tend à confirmer cette hypothèse %
  % Ces catégories servent probablement à définir des limites %

## Exporting values
# Defining nodes
##nodes_vect={}
##for i = 7:length(nodes)
##  n=node(nodes(i,1),nodes(i,2),nodes(i,3),nodes(i,4));
##  nodes_vect = {nodes_vect,n};
## end
 
# Defining contact triangles elements

# Defining membrane_3 elements

# Defining boundary conditions

## Some other parameters:
m1=material();
m2=material();

## Writing .bim file
directory = 'double_dome_forming.bim'
fid=fopen(directory,'w');
# Header
fprintf(fid, ['CONTROLS' '\n']);
fprintf(fid, ['RUN FROM 0.0 TO 5.0000000' '\n']);
fprintf(fid, ['PRINT EVERY 0.0500000'   '\n']);
fprintf(fid, ['\n']);
# Material
fprintf(fid, ['MATERIALS TYPE HYPERTEXTILE' '\n']);
fprintf(fid, [m1.name ' RHO = ' num2str(m1.rho) ' DAMPING = ' num2str(m1.damping) ' WARPORI = ' m1.warpori ' WEFTORI = ' m1.weftori ' KELONGWARP = ' m1.kelongwarp ' KELONGWEFT = ' m1.kelongweft ' KSHEAR =' m1.kshear '\n']);
fprintf(fid, ['\n\n']);
# Type of constraints (displacement boundary conditions) %comment/ask symmetric conditions
fprintf(fid, ['CONSTRAINTS TYPE BOUNDARY_CONDITION' '\n']);
fprintf(fid, ['BC_Set_symx VX = 0.0000000 AX = 0.0000000 VRY = 0.0000000 ARY = 0.0000000 VRZ = 0.0000000 ARZ = 0.0000000' '\n']);
fprintf(fid, ['BC_Set_symmy VY = 0.0000000 AY = 0.0000000 VRX = 0.0000000 ARX = 0.0000000 VRZ = 0.0000000 ARZ = 0.0000000' '\n']);
fprintf(fid, ['BC_Set_RP_Punch VX = 0.0000000 VY = 0.0000000 VZ = -10.0' '\n']);
fprintf(fid, ['Mix_BC_Set_symxBC_Set_symmy VX = 0.0000000 AX = 0.0000000 VRY = 0.0000000 ARY = 0.0000000 VRZ = 0.0000000 ARZ = 0.0000000 VY = 0.0000000 AY = 0.0000000 VRX = 0.0000000 ARX = 0.0000000' '\n']);
fprintf(fid, ['\n\n']);
# Nodes 
fprintf(fid, ['NODES' '\n']);
for i = 7:length(nodes)
  n=node(nodes(i,1),nodes(i,2),nodes(i,3),nodes(i,4));
  fprintf(fid,'%d \t X= %f \t Y= %f \t Z= %f \n', n.nbr,n.x,n.y,n.z);
end
# Elements
fprintf(fid, ['\n']);
fprintf(fid, ['ELEMENTS TYPE  CONTACT_TRIANGLE' '\n']);
##for e = elements_vect
##  fprintf(fid,['%d\tnodes= [%d,%d,%d] T = 1.0000000	FACTOR = 10.0', e.nbr,e.n1,e.n2,e.n3 '\n']);
##end
fclose(fid)


