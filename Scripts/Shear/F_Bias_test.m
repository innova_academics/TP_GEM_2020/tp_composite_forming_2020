function F=F_Bias_test(Vd,p,c)


%**************************************************************************
% -------- VARIABLES D'ENTREE ---------
%  Vd    = variables dependents
% >> Vd(:,1) : Temps
% >> Vd(:,1) : Deplacement  
%  p     = vecteur comprenant les paramètres à optimiser
%
% ---------- VARIABLES DE SORTIE -------
% F  = Vecteur effort machine.
%**************************************************************************


d   = Vd(:,1);

   

% Parametres geometriques.
Lo   = 210 ;              %Largeur
La   = 70;              %Longueur
ho   = 1.0 ;               %Epaisseur initiale (mm)
D    = (Lo-La);          %Infield diagonal
S1   = La*(Lo -3/2*La);  %Area sheared zone
S2   = La^2;             %Demi area sheared zone
PI   = 4*atan(1);

%Formule de l'angle de cisaillement théorique gamma
Gamathr = PI/2 - 2*acos( ( sqrt(2)/2 ) * ( (D+d)/D ) );



%% Dans la ZONE A : Zone en cisaillement pur au cnetre de l'éprouvette :

    %Calcul de la densité d'énergie de déformation ainsi que de sa dérivée
    
        %Invariant de cisaillement
        I = sin(Gamathr);

        %C'est quoi ça ?
        alpha_r = acos(I);
        [~, Dw] = w(4,I,p ); %Utilisation de [...]

    %Calcul du tenseur de Piola Kirchoff 2 :

    S11 = -(1+I).*Dw;
    S22 =  (1-I).*Dw;

    %Détermination de la force F :

    f1 = ((S22 - S11).*(cos(Gamathr)).*ho)./(sqrt(2).*sin(alpha_r/2).*D);

%% Dans la ZONE B 

    %Calcul de la densité d'énergie de déformation ainsi que de sa dérivée
        
        %Invariant de cisaillement
        I = sin(Gamathr/2);

        %C'est quoi ça ?
        [~, Dw] = w(4,I,p ) ;

    %Calcul du tenseur de Piola Kirchoff 2 :

    S11 = -(1+I).*Dw;
    S22 =  (1-I).*Dw;

    %Détermination de la force F approchée dans la zone B :

    f2 = ((S22 - S11).*(cos(Gamathr/2)).*ho)./(2*sqrt(2).*sin(alpha_r/2).*D);


%% Calcul de la force totale : simple somme sur les deux zones !

 F = S1*f1 + f2*S2 ;


end
