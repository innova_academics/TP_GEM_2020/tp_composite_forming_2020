%% Routine to add textile mesh in entry file "*.bim"
%% Written by G. Lebreton and N. Piecko - 20/01/2021
clear all
# Mesh parameters for membrane
l = 270 ; %half-length
h = 195 ; %half-width
l_e = 10 ; %same length in x and y
[X,Y]=meshgrid(0:l_e:l,0:l_e:h);

## Writing .bim file
directory_p = 'Geom_01_Double_Dome_00_Mesh_double_dome_tools.bim';
parent=fopen(directory_p,'r');
directory = 'double_dome_forming.bim';
fid=fopen(directory,'w');
# copy 
line=0;
while ~feof(parent) % feof(fid) is true when the file ends
  line =line +1;
  if (line == 1508)
    # nodes
%    fprintf(fid, ['\n *to move - node* \n']);
    k_node = 1490;
    k_n_init = 1490;
    for i = Y(:,1)'  
      for j = X(1,:)
        k_node = k_node +1;
        if (i==0) && (j==0)
          fprintf(fid,' %d \tX=\t%.2f\tY=\t%.2f\tZ=\t%.2f\tConstraint = %s\n', k_node,j,i,0,'FIXED_XY')
        elseif i==0
          fprintf(fid,' %d \tX=\t%.2f\tY=\t%.2f\tZ=\t%.2f\tConstraint = %s\n', k_node,j,i,0,'FIXED_Y')
        elseif j==0
          fprintf(fid,' %d \tX=\t%.2f\tY=\t%.2f\tZ=\t%.2f\tConstraint = %s\n', k_node,j,i,0,'FIXED_X')        
        else 
          fprintf(fid,' %d \tX=\t%.2f\tY=\t%.2f\tZ=\t%.2f\n', k_node,j,i,0)
         end 
   end
    end    
    
  end
      textLineEntry = fgetl(parent); % read one line
      fprintf(fid, [textLineEntry '\n']);
end

# add a new membrane3-like 
fprintf(fid, ['\n ELEMENTS TYPE MEMBRANE_3 \n']);
k_element=2770;
k_e_init=2770;
T=[[]];
for i = 1:size(X)(1)-1
  for j = 1:size(Y)(2)-1
    k_element = k_element +1;
    n1 = k_n_init+j+ (i-1)*size(X)(2);
    n2 = k_n_init+j+ (i)*size(X)(2);
    n3 = k_n_init+j+ (i)*size(X)(2)+1;
    n4 = k_n_init+j+ (i-1)*size(X)(2)+1;
    T=[T;[n1 n2 n3];[n1 n3 n4]];
    fprintf(fid,' %d\tnodes = [%d,%d,%d] \t MATERIAL = %s	T = 1.0000000	FACTOR = 10.0	CONTACT = EDGE\n', k_element,n1,n2,n3,'HyperTex_90');
    k_element = k_element +1;
    fprintf(fid,' %d\tnodes = [%d,%d,%d] \t MATERIAL = %s	T = 1.0000000	FACTOR = 10.0	CONTACT = EDGE\n', k_element,n1,n3,n4,'HyperTex_90');
end
end


fclose(fid);fclose(parent);
