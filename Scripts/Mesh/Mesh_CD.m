function [coord, lnods] = Mesh_CD(xmin,xmax,ymin,ymax,z,nelemc,neleml,nnoeudavant,nelemavant,nbody)

npoints = (neleml+1)*(nelemc+1) + neleml *nelemc ;  % nombre de noueds
nelemen = neleml*nelemc*4; 
coord = zeros(npoints,4);
lnods = zeros(nelemen,4);
compteur=0;
pos=nnoeudavant;
dx =(xmax-xmin)/neleml; 
dy =(ymax-ymin)/nelemc;

for  colon=1:nelemc+1 
    for ligne=1:neleml+1
        compteur =compteur+1;
        pos = pos+1;
        coord(compteur,1)=pos; %numero du noeud
        coord(compteur,2)=xmin +(ligne-1)*dx;
        coord(compteur,3)=ymin+(colon-1)*dy;
        coord(compteur,4)=z;    %coordonnées selon z
    end    
    if colon <= nelemc
    for ligne=1:neleml
        compteur =compteur+1;
        pos = pos+1;
        coord(compteur,1)=pos; %numero du noeud
        coord(compteur,2)=xmin +(ligne-1)*dx+0.5*dx;
        coord(compteur,3)=ymin +(colon-1)*dy+0.5*dy;
        coord(compteur,4)=z;    %coordonnées selon z
    end
    end
end

pos=nelemavant;
compteur=0; 
    for  colon=1:nelemc
        for ligne=1:neleml
            N1 =(colon-1)*(neleml+1)+(colon-1)*(neleml)+ligne  +nnoeudavant;  
            N2 =(colon-1)*(neleml+1)+(colon-1)*(neleml)+ligne+1+nnoeudavant;
            N3 =(colon)*(neleml+1)+(colon)*(neleml)+ligne+1+nnoeudavant;
            N4 =(colon)*(neleml+1)+(colon)*(neleml)+ligne+nnoeudavant;
            N5 =(colon-1)*(neleml+1)+(colon-1)*(neleml)+ligne+neleml+1+nnoeudavant  ; 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       triangle 1  
            compteur =compteur+1;
            pos = pos+1;
            lnods(compteur,1)=pos;          
            lnods(compteur,2)= N1;
            lnods(compteur,3)= N2;
            lnods(compteur,4)= N5;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       triangle 2        
            compteur =compteur+1;
            pos = pos+1;
            lnods(compteur,1)=pos;            
            lnods(compteur,2)= N2;
            lnods(compteur,3)= N3;
            lnods(compteur,4)= N5;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       triangle 3        
            compteur =compteur+1;
            pos = pos+1;
            lnods(compteur,1)=pos; 
            lnods(compteur,2)= N3;
            lnods(compteur,3)= N4;
            lnods(compteur,4)= N5;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       triangle 4        
            compteur =compteur+1;
            pos = pos+1;
            lnods(compteur,1)=pos;
            lnods(compteur,2)= N4;
            lnods(compteur,3)= N1;
            lnods(compteur,4)= N5;

        end
    end
end     