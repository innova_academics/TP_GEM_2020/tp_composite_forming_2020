% @Brief  : Create and instersection between a polygon and an structured
%           mesh. Compatible with Matlab and Octave.  
% @Author : Eduardo Guzman Maldonado
% @Copyrigth: Innovamics
%
% -----------------------------------------------------------------------
%                    Clear work space
% -----------------------------------------------------------------------
% Operations de nettoyage
format compact; % viewing preference
clear variables;
clc;

% -----------------------------------------------------------------------
%                    Input Data
% -----------------------------------------------------------------------


% Maximal size of the rectangular mesh
xmin=0;   xmax=100;
ymin=0;   ymax=100;

%Mesh density
nelemx = 10;       % number of element in x direction

% Type of mesh 
% 1 - CD 
% 2 - BI 45
% 3 - BI 135
% 4 - BI Random
% 5 - BI 45 135
Type_mesh = 1; 

% Translation


% -----------------------------------------------------------------------
%                    Mesh generation
% -----------------------------------------------------------------------

% Size of the elements 
esize = (xmax - xmin) / nelemx;

% keep the ratio for the elements
nelemy = round((ymax - ymin)/(esize));% number of element in y direction

% Generate structured mesh
if(Type_mesh==1)
    [nodes, elements] = Mesh_CD(xmin,xmax,ymin,ymax,0,nelemy,nelemx,0,0,1);
elseif (Type_mesh==2)
    [nodes, elements] = Mesh_Bi(xmin,xmax,ymin,ymax,0,nelemy,nelemx,0,0,2);
elseif (Type_mesh==3)
    [nodes, elements] = Mesh_Bi(xmin,xmax,ymin,ymax,0,nelemy,nelemx,0,0,3);
elseif (Type_mesh==4)
    [nodes, elements] = Mesh_Bi(xmin,xmax,ymin,ymax,0,nelemy,nelemx,0,0,4);
elseif (Type_mesh==5)
    [nodes, elements] = Mesh_Bi_45(xmin,xmax,ymin,ymax,0,nelemy,nelemx,0,0,2);
end
% -----------------------------------------------------------------------
%                    Write vtk file
% -----------------------------------------------------------------------
if(Type_mesh==1)
% VTK input file name
file_vtk=strcat('Plaque_CD',num2str(abs(xmin-xmax)),'x',num2str(abs(xmin-xmax)),'.vtk');
elseif(Type_mesh==2)
% VTK input file name
file_vtk=strcat('Plaque_BI45',num2str(abs(xmin-xmax)),'x',num2str(abs(xmin-xmax)),'.vtk');
elseif(Type_mesh==3)
% VTK input file name
file_vtk=strcat('Plaque_BI135',num2str(abs(xmin-xmax)),'x',num2str(abs(xmin-xmax)),'.vtk');
elseif(Type_mesh==4)
% VTK input file name
file_vtk=strcat('Plaque_BIAL',num2str(abs(xmin-xmax)),'x',num2str(abs(xmin-xmax)),'.vtk');
elseif(Type_mesh==5)
% VTK input file name
file_vtk=strcat('Plaque_BIAL',num2str(abs(xmin-xmax)),'x',num2str(abs(xmin-xmax)),'.vtk');
end

% -----------------------------------------------------------------------
% VTK writing
% -----------------------------------------------------------------------
% VTK file format:
% # vtk DataFile Version 3.0              >> Header
% ASCII                                   >> Data type
% DATASET UNSTRUCTURED_GRID               >> Geometrie Topology
% POINTS  n datatype                      >> Dataset attributes  
% x0   y0   z0
% x1   y1   z1
% x2   y2   z2
% xn-1 yn-1 zn-1
% CELLS  n size
% numpoints  p1 p2 p3 ...
% CELLS TYPES n
% type
% type
% type
npoints = length(nodes);
nelemen = length(elements);

fid = fopen(file_vtk, 'w');
if fid == -1
    error('Cannot open file for writing.');
end
fprintf(fid,'# vtk DataFile Version 3.0\n');
fprintf(fid,'Mesh visualiation\n');
fprintf(fid,'ASCII\n');
fprintf(fid,'DATASET UNSTRUCTURED_GRID\n');
fprintf(fid,'POINTS %5d float\n',npoints+1);
fprintf(fid,'%3.7f   %3.7f   %3.7f \n',0, 0, 0);
fprintf(fid,'%3.7f   %3.7f   %3.7f \n',(nodes(:,2:4))');
fprintf(fid,'CELLS %d %d \n',nelemen,nelemen*4);
fprintf(fid,'3 %5d %5d %5d  \n',(elements(:,2:4))' );
fprintf(fid,'CELL_TYPES  %d \n',nelemen);
fprintf(fid,'%d \n',ones(1,nelemen)*5 );
fclose(fid);