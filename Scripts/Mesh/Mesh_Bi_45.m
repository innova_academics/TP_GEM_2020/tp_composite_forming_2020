function [coord, lnods] = Mesh_Bi_45(xmin,xmax,ymin,ymax,z,nelemc,neleml,nnoeudavant,nelemavant,nbody)

npoints = (neleml+1)*(nelemc+1);  % nombre de noueds
nelemen = neleml*nelemc*2; 
coord = zeros(npoints,4);
lnods = zeros(nelemen,4);
compteur=0;
pos=nnoeudavant;
dx =(xmax-xmin)/neleml; 
dy =(ymax-ymin)/nelemc;
% -----------------------------------------------------------------------
% Nuage de Points
for  colon=1:nelemc+1 
    for ligne=1:neleml+1
        compteur =compteur+1;
        pos = pos+1;
        if mod(colon,2)
            coord(compteur,1)=pos; %numero du noeud
            coord(compteur,2)=xmin +(ligne-1)*dx+ dx/2  ;
            coord(compteur,3)=ymin +(colon-1)*dy ;
            coord(compteur,4)=z;    %coordonnées selon z
            coord(compteur,5)=nbody;   %ibody
        else
            coord(compteur,1)=pos; %numero du noeud
            coord(compteur,2)=xmin +(ligne-1)*dx ;
            coord(compteur,3)=ymin +(colon-1)*dy;
            coord(compteur,4)=z;    %coordonnées selon z
            coord(compteur,5)=nbody;   %ibody           
        end  
    end    
end
% -----------------------------------------------------------------------
% Creation du maillage 

pos=nelemavant;
compteur=0; 
    for  colon=1:nelemc
        for ligne=1:neleml

            N1 =(colon-1)*(neleml+1)+ligne  +nnoeudavant;  
            N2 =(colon-1)*(neleml+1)+ligne+1+nnoeudavant;
            N3 =(colon)*(neleml+1)+ligne+1+nnoeudavant;
            N4 =(colon)*(neleml+1)+ligne+nnoeudavant;
         
            if ~mod(colon,2)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       triangle 1  
            compteur =compteur+1;
            pos = pos+1;
            lnods(compteur,1)=pos;        
            lnods(compteur,2)= N1;
            lnods(compteur,3)= N2;
            lnods(compteur,4)= N4;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       triangle 2        
            compteur =compteur+1;
            pos = pos+1;
            lnods(compteur,1)=pos;            
            lnods(compteur,2)= N2;
            lnods(compteur,3)= N3;
            lnods(compteur,4)= N4;
            else
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       triangle 1  
            compteur =compteur+1;
            pos = pos+1;
            lnods(compteur,1)=pos;         
            lnods(compteur,2)= N1;
            lnods(compteur,3)= N2;
            lnods(compteur,4)= N3;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %       triangle 2        
            compteur =compteur+1;
            pos = pos+1;
            lnods(compteur,1)=pos;
            lnods(compteur,2)= N3;
            lnods(compteur,3)= N4;
            lnods(compteur,4)= N1;
            end
        end
    end

end     