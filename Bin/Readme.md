# Comfor 0.1.0

Comfor est un logiciel prototype pour la modélisation de la mise en forme de composites. Les executables dans ce dossier permettent de lancer une instance du logiciel sans installation locale.

- COMFOR : Version mono processeur
- COMFOR Parallèle : Version beta pour le lancement des calculs parallélisés.

## Lancement

Pour lancer le calcul il suffit de lancer l'executables depuis le terminal:

```bash
> ./COMFOR
```

Le logiciel demande ensuite le nom du fichier d'entrée.

## Mise en données

Le fichier d'entrée est un fichier texte brut. L'extension par défaut est **\*.bim** (basic bloc input model) mais elle n'est pas prise en compte par le logiciel.

### Format

La mise en données est fournie par blocs. La position de ces blocs dans le fichier d'entrée n'a aucune importance.
