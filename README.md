# Projet d'option - 2020

## Sujet

Développement d'un TP numérique de formage des composites.

## Auteurs

- C. Pinot
- N. Piecko
- G. Lebreton
- V. Raffault

## Encadrants

- C. Binetruy
- E. Guzman

## Avancement

```mermaid
gantt
    title Avancement projet
    dateFormat  YYYY-MM-DD
    axisFormat %d/%m
    section Global
    Demarrage : 2020-11-18, 1d
    1ere réunion : 2020-11-23,1d
    2eme réunion : 2020-12-07, 1d
    1 point avancement : 2020-12-14,1d
    Fin: 2021-04-14, 1d
    section Biblio
    Lecture      : 2020-11-23  , 14d
    section Numérique
    Prise en main logiciel : 2020-12-07, 21d
    Simulation formage double dôme : 21d
    section Essai de caractérisation 
    Essai caractérisation textile : 14d
    section Travail pratique
    Rédaction: 7d
    Validation: 7d
    Correction: 7d
```

 ## Créneaux 
```mermaid
gantt
    title Créneaux de travail projet
    dateFormat  YYYY-MM-DD
    axisFormat %d/%m
    Section Créneaux
    C1: 2020-11-16, 1d    
        C2: 2020-11-23, 1d
        C3: 2020-11-30, 1d
        C4: 2020-12-07, 1d
        C5: 2020-12-14, 1d
    C6: 2021-01-04, 1d
        C7: 2021-01-15, 1d
        C8: 2021-01-18, 1d
        C9: 2021-01-20, 1d
        C10: 2021-01-22, 1d
        C11: 2021-01-25, 1d
        C12: 2021-01-29, 1d
    C13: 2021-02-03, 1d
        C14: 2021-02-10, 1d
        C15: 2021-02-17, 1d
    C16: 2021-03-10, 1d
        C17: 2021-03-24, 1d
        C18: 2021-03-29, 1d
    C19: 2021-04-02, 1d

```



